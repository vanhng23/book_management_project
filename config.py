import os
from dotenv import load_dotenv

load_dotenv('.env')

ENV = os.getenv('ENV', 'app_code')
APP_CODE = os.getenv('APP_CODE', 'app_code')
APP_ID = os.getenv('APP_ID', 'app_id')
SECRET_KEY = os.getenv('SECRET_KEY', 'secret_key')

FRONTEND_DOMAIN = os.getenv('FRONTEND_DOMAIN', 'local.airasia.com')
API_DOMAIN = os.getenv('API_DOMAIN', 'local.airasia.com')

POSTGRES_URI = os.getenv('POSTGRES_URI', 'postgres_uri')
MONGO_URI = os.getenv('MONGO_URI', 'localhost')

CORE_URL = os.getenv('CORE_URL', 'http://localhost:5002')
STORAGE_URL = os.getenv('STORAGE_URL', 'http://localhost:5002')
IAM_URL = os.getenv('IAM_URL', 'http://localhost:5001')
AIRASIA_SSO_URL = os.getenv('AIRASIA_SSO_URL', 'https://ssor.stgairasia.com')

DATADOG_HOST = os.getenv('DATADOG_HOST', '34.87.52.127')
DATADOG_PORT = os.getenv('DATADOG_PORT', 8126)
DATADOG_ENABLE = os.getenv('DATADOG_ENABLE', True)


class ApiConfig(object):
    DEBUG = True

    MAX_CONTENT_LENGTH = 15 * 1024 * 1024

    PROPAGATE_EXCEPTIONS = True


class CeleryConfig(object):
    pass
