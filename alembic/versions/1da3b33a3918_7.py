"""7

Revision ID: 1da3b33a3918
Revises: 81b2f17e9977
Create Date: 2023-01-09 22:59:33.520514

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '1da3b33a3918'
down_revision = '81b2f17e9977'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('passenger', sa.Column('user', sa.String(length=36), nullable=False))
    op.add_column('passenger', sa.Column('password', sa.String(length=100), nullable=False))
    op.drop_index('ix_passenger_passenger_code', table_name='passenger')
    op.create_index(op.f('ix_passenger_password'), 'passenger', ['password'], unique=False)
    op.create_index(op.f('ix_passenger_user'), 'passenger', ['user'], unique=False)
    op.drop_column('passenger', 'passenger_code')
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('passenger', sa.Column('passenger_code', sa.VARCHAR(length=36), autoincrement=False, nullable=False))
    op.drop_index(op.f('ix_passenger_user'), table_name='passenger')
    op.drop_index(op.f('ix_passenger_password'), table_name='passenger')
    op.create_index('ix_passenger_passenger_code', 'passenger', ['passenger_code'], unique=False)
    op.drop_column('passenger', 'password')
    op.drop_column('passenger', 'user')
    # ### end Alembic commands ###
