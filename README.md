# Goquo App Backend

## Requirements
- Ubuntu 18.04
- Python 3.6+

## Configuration
Create .env file in backend's root directory
```.env
APP_ID=app_id

APP_ENV_ID=app_env_id

APP_NAME=app_name

SECRET_KEY=secret_key

POSTGRES_URI=postgresql://localhost:5432/db_name

CORE_ENGINE_PUBLIC_URL=http://localhost:5001

STATIC_DATA_URL=http://localhost:5001

REDIS_PASSWORD=password
REDIS_HOST=localhost
REDIS_PORT=6379


```

## Setup
`export PYTHONPATH=$PWD`

`pip3 install pipenv`

`pipenv shell`

`export $(cat .env | xargs)`

`alembic upgread head`

`python run.py init-db`

## Run
##### End user api
`pipenv run python run.py api --enable-uwsgi=true --api-file=end_user_api.py --host=127.0.0.1 --port=3001 --processes=2 --threads=5`

##### Celery worker
`python run.py celery-worker`

##### Api session cleaner
`python run.py cronjob api-session-cleaner`

## DOC
https://documenter.getpostman.com/view/1752453/SW14Uwsa

