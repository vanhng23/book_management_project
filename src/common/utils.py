import logging
import jwt
import json
import hashlib
import secrets
from collections import OrderedDict
from datetime import datetime, timedelta
from cryptography.fernet import Fernet
from urllib.parse import urlparse

from .constants import PAGINATION, VALID_DATETIME_FORMATS

log = logging.getLogger('EASYGDS')


def make_loggable_data(data):
    length = 1000
    result = str(data)

    if len(result) > length:
        result = result[:length] + '...'

    return result


def log_data(mode: str,
             template: str,
             args: list = None,
             kwargs: dict = None):
    handler = getattr(log, mode)

    if not args:
        args = []
    if not kwargs:
        kwargs = {}

    _args = [
        make_loggable_data(i)
        for i in args
    ]

    _kwargs = dict(list(map(
        lambda i: (i[0], make_loggable_data(i[1])),
        kwargs.items()
    )))

    handler(template.format(*_args, **_kwargs))


def get_url_origin(url):
    parsed = urlparse(url)
    return '{schema}://{netloc}'.format(
        schema=parsed.scheme,
        netloc=parsed.netloc
    )


def encrypt(string, secret_key):
    if not isinstance(secret_key, bytes):
        secret_key = secret_key.encode()

    fernet = Fernet(secret_key)

    if not isinstance(string, bytes):
        string = string.encode()
    return fernet.encrypt(string).decode()


def decrypt(token, secret_key):
    if not isinstance(secret_key, bytes):
        secret_key = secret_key.encode()

    fernet = Fernet(secret_key)

    if not isinstance(token, bytes):
        token = token.encode()
    return fernet.decrypt(token).decode()


def get_now(timestamp=False):
    result = datetime.utcnow()
    if timestamp:
        return result.timestamp()
    return result


def paginate(query, page=None, per_page=None):
    if not page:
        page = PAGINATION['page']
    if not per_page:
        per_page = PAGINATION['per_page']
    return query.offset((page - 1) * per_page).limit(per_page)


def convert_string_to_datetime(string, formats):
    value = None
    for datetime_format in formats:
        try:
            value = datetime.strptime(string, datetime_format)
            break
        except ValueError:
            continue

    return value


def make_jwt_token(secret_key, expire_in=None, **kwargs):
    now = datetime.now()

    if expire_in:
        expire = now + timedelta(seconds=expire_in)

    else:
        expire = now + timedelta(seconds=7200)

    payload = OrderedDict(
        exp=expire,
        **kwargs
    )
    return jwt.encode(payload, secret_key, algorithm='HS256').decode()


def decode_jwt_token(token,
                     secret_key=None,
                     verify_exp=False,
                     verify_signature=True,
                     ):
    if verify_signature and not secret_key:
        raise Exception('Missing secret_key')

    payload = dict(
        jwt=token,
        verify=verify_signature,
        options={'verify_exp': verify_exp}
    )

    try:
        token_data = jwt.decode(**payload)

    except Exception as e:
        return None
    return token_data


def hash_data(data):
    if not isinstance(data, bytes):
        if not isinstance(data, str):
            data = json.dumps(data)
        data = data.encode()

    return hashlib.md5(data).hexdigest()


def hash_string(string, mode='sha256'):
    hash_handler = getattr(hashlib, mode, None)
    if not hash_handler:
        raise Exception('UnsupportedHashMode')

    if not isinstance(string, bytes):
        string = string.encode()

    return hash_handler(string).hexdigest()


def find_list_element_obj(array: list,
                          key: str,
                          value: any) -> tuple:
    result = None
    index = None
    for i, item in enumerate(array):
        if isinstance(item, dict):
            v = item[key]
        elif isinstance(item, object):
            v = getattr(item, key)
        else:
            v = item
        if v != value:
            continue

        result = item
        index = i
        break
    return result, index


def find_in_list(array: list,
                 values: dict):
    first_item = array[0]

    def check_function(item):
        match = True
        for k, v in values.items():
            if isinstance(first_item, dict):
                if k not in item:
                    match = False
                    break
                item_value = item[k]
            else:
                if not hasattr(item, k):
                    match = False
                    break
                item_value = getattr(item, k)
            if v != item_value:
                match = False
                break
        return match

    query = list(filter(lambda x: check_function(x), array))
    if not len(query):
        return None, None
    result = query[0]
    return result, array.index(result)


def get_key_by_value(data: dict, value: any):
    values = list(data.values())
    keys = list(data.keys())
    try:
        index = values.index(value)
    except ValueError:
        index = None
    if index is None:
        return None
    return keys[index]


def gen_random_token_hex(n: int = 16):
    return secrets.token_hex(n)


def change_datetime_format(string, to_format):
    datetime_obj = convert_string_to_datetime(string, VALID_DATETIME_FORMATS)
    return datetime_obj.strftime(to_format)


def mongo_sort(sorts):
    if not sorts:
        sorts = ['-created_at']

    sort_list = []

    for string in sorts:
        # get key, value of sort item
        try:
            key, value = string.split('==')
        except ValueError:
            key = string
            value = None

        # get column name and descending
        if key.startswith('-'):
            field_name = key[1:len(key)]
            descending = True
        else:
            descending = False
            if string.startswith('+'):
                field_name = key[1:len(key)]
            else:
                field_name = key

        if descending:
            sort_list.append((field_name, -1))
        else:
            sort_list.append((field_name, 1))

    return sort_list
