import inspect
import sentry_sdk
import logging
import os
import importlib
from flask import Flask, make_response, jsonify, g
from flask_cors import CORS
from werkzeug.exceptions import HTTPException

from src.bases.error.api import HTTPError
from src.common.utils import log

from .resource import Resource


class Factory(object):
    def __init__(self,
                 config,
                 mongo_db,
                 sql_session_factory=None,
                 resource_module=None,
                 app_name=None,
                 error_handler=None,
                 request_callback=None,
                 response_callback=None):

        self.config = config
        self.mongo_db = mongo_db
        self.sql_session_factory = sql_session_factory
        self.resource_module = resource_module
        self.app_name = app_name or 'Flask'
        self.error_handler = error_handler
        self.request_callback = request_callback
        self.response_callback = response_callback

    @staticmethod
    def default_error_handler(app):
        @app.errorhandler(Exception)
        def handle_error(e):
            if isinstance(e, HTTPError):
                status_code = e.status_code
                data = e.output()
            elif isinstance(e, HTTPException):
                status_code = e.code
                data = e.__class__.__name__
            else:
                status_code = 500
                if app.debug:
                    raise e
                data = dict(message='Server error - %s' % e)

            if status_code >= 500:
                sentry_sdk.capture_exception(e)

            return make_response(jsonify(data), status_code)

    def install_resource(self, app):
        if not self.resource_module:
            return

        resource_classes = set()
        rs_root_pack = self.resource_module.__name__.split('.')
        rs_root_dir = os.path.dirname(self.resource_module.__file__)
        for dir_path, dir_names, file_names in os.walk(rs_root_dir):
            diff = os.path.relpath(dir_path, rs_root_dir)
            if diff == '.':
                diff_dirs = []
            else:
                diff_dirs = diff.split('\\')
            target_pack_prefix = rs_root_pack + diff_dirs
            for dir_name in dir_names:
                target_pack = target_pack_prefix + [dir_name]
                module = importlib.import_module('.'.join(target_pack))
                classes = inspect.getmembers(module,
                                             inspect.isclass)
                for cls_name, cls in classes:
                    resource_classes.add(cls)

        for cls in resource_classes:
            if not issubclass(cls, Resource):
                continue

            # ignore resources those have none endpoint attr
            if not cls.endpoint:
                continue

            endpoint = cls.endpoint
            # if cls.endpoint_prefix:
            #     endpoint = cls.endpoint_prefix + endpoint

            app.add_url_rule(endpoint,
                             view_func=cls.as_view(cls.__name__))

    def setup_logging(self):
        ch = logging.StreamHandler()
        ch.setFormatter(logging.Formatter("%(asctime)s --  %(levelname)s  -- %(message)s"))
        log.setLevel(logging.DEBUG)
        log.addHandler(ch)

    def create_app(self):
        app = Flask(__name__)

        '''Cross origin'''
        CORS(app, supports_credentials=True,
             automatic_options=True)

        '''Load config'''
        app.config.from_object(self.config)

        '''Error handling configuration'''
        error_handler = self.error_handler or self.default_error_handler
        error_handler(app)

        '''Callbacks configuration'''
        @app.before_request
        def handle_before_request():
            g.sql_session = self.sql_session_factory()
            g.mongo_db = self.mongo_db

        @app.after_request
        def handle_after_request(response):
            self.sql_session_factory.remove()
            return response

        '''Resources installation'''
        self.install_resource(app)

        self.setup_logging()

        return app
