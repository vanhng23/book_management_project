from src.models import Passenger
from src.bases.api.method_handler import MethodHandler
from src.bases.error.api import BadRequestParams, ServerError
from src.bases.error.core import CoreError
from .schemas import GetSchema, PostSchema, PatchSchema

class Get(MethodHandler):
    input_schema_class = GetSchema
    def _handle_api_logic(self):
        listing_passenger = self.session.query(Passenger).all()
        return [psg.to_json() for psg in listing_passenger]
    
class Post(MethodHandler):
    input_schema_class = PostSchema
    def _handle_api_logic(self):
        print(self.payload)
        user = self.payload.get('user')
        password = self.payload.get('password')
        first_name = self.payload.get('first_name')
        last_name = self.payload.get('last_name')
        email = self.payload.get('email')
        gender = self.payload.get('gender')
        status = self.payload.get('status')
        
        result = self._create(user, password, first_name, last_name, email, gender, status)
        self.session.commit()
        return result
        
        
    def _create(self,
               user: str,
               password: str,
               first_name: str,
               last_name: str,
               email: str,
               gender: str,
               status: str
               ) -> Passenger:

        passenger = Passenger()
        passenger.user = user
        passenger.password = password
        passenger.first_name = first_name
        passenger.last_name = last_name
        passenger.email = email
        passenger.gender = gender
        passenger.status = status
        
        self.session.add(passenger)
        return passenger.to_json()

class Patch(MethodHandler):
    input_schema_class = PatchSchema
    def _handle_api_logic(self):
        passenger_id = self.payload.get('id')

        passenger = self.session.query(Passenger).filter(
            Passenger.id == passenger_id
        ).first()

        if not passenger:
            raise BadRequestParams(message='Passenger not found')

        try:
            result = self._update_passenger(
                passenger = passenger,
                user = self.payload.get('user'),
                password = self.payload.get('password'),
                first_name = self.payload.get('first_name'),
                last_name = self.payload.get('last_name'),
                email = self.payload.get('email'),
                gender = self.payload.get('gender'),
                status = self.payload.get('status')
            )
        except CoreError as e:
            if e.error == 'InvalidParams':
                raise BadRequestParams(message=e.message)
            raise ServerError(e.error, e.message)
        self.session.commit()
        return result.to_json()
    
    def _update_passenger(self,
               passenger: Passenger,
               user: str,
               password: str,
               first_name: str,
               last_name: str,
               email: str,
               gender: str,
               status: str
               ) -> Passenger:

        passenger.user = user
        passenger.password = password
        passenger.first_name = first_name
        passenger.last_name = last_name
        passenger.email = email
        passenger.gender = gender
        passenger.status = status
        
        self.session.add(passenger)
        return passenger
        