from src.bases.api.resource import Resource


class PassengerResource(Resource):
    endpoint = '/passenger'
