from src.bases.schema import (BaseListingSchema, BaseCreatingSchema, BaseUpdatingSchema,
                              fields, StringField, STRING_LENGTH_VALIDATORS)

class GetSchema(BaseListingSchema):
    pass

class PostSchema(BaseCreatingSchema):
    user = StringField(required=True, validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])
    
    password = StringField(required=True, validate=STRING_LENGTH_VALIDATORS['LONG'])
    
    first_name = StringField(required=True, validate=STRING_LENGTH_VALIDATORS['LONG'])
    
    last_name = StringField(required=True, validate=STRING_LENGTH_VALIDATORS['LONG'])
    
    email = StringField(validate=STRING_LENGTH_VALIDATORS['LONG'])

    gender = StringField(validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])

    status = StringField(validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])

class PatchSchema(BaseUpdatingSchema):
    user = StringField(required=True, validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])
    
    password = StringField(required=True, validate=STRING_LENGTH_VALIDATORS['LONG'])
    
    first_name = StringField(required=True, validate=STRING_LENGTH_VALIDATORS['LONG'])
    
    last_name = StringField(required=True, validate=STRING_LENGTH_VALIDATORS['LONG'])
    
    email = StringField(validate=STRING_LENGTH_VALIDATORS['LONG'])

    gender = StringField(validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])

    status = StringField(validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])