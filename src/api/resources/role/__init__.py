from src.bases.api.resource import Resource


class RoleResource(Resource):
    endpoint = '/role'
