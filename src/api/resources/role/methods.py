from src.models import Role
from src.bases.api.method_handler import MethodHandler
from src.bases.error.api import BadRequestParams, ServerError
from src.bases.error.core import CoreError
from .schemas import GetSchema, PostSchema, PatchSchema

class Get(MethodHandler):
    input_schema_class = GetSchema
    def _handle_api_logic(self):
        rol = self.session.query(Role).all()
        return [r.to_json() for r in rol]
    
class Post(MethodHandler):
    input_schema_class = PostSchema
    def _handle_api_logic(self):
        name = self.payload.get('name')
        description = self.payload.get('description')
        enabled = self.payload.get('enabled')
        
        result = self._create(name, description, enabled)
        self.session.commit()
        return result
        
        
    def _create(self,
               name: str,
               description: str,
               enabled: bool
               ) -> Role:

        role = Role()
        role.name = name
        role.description = description
        role.enabled = enabled
        
        self.session.add(role)
        return role.to_json()

class Patch(MethodHandler):
    input_schema_class = PatchSchema
    def _handle_api_logic(self):
        role_id = self.payload.get('id')

        rol = self.session.query(Role).filter(
            Role.id == role_id
        ).first()

        if not rol:
            raise BadRequestParams(message='Role not found')

        try:
            result = self._update_role(
                rol = rol,
                name = self.payload.get('title'),
                description = self.payload.get('description'),
                enabled = self.payload.get('enabled')
            )
        except CoreError as e:
            if e.error == 'InvalidParams':
                raise BadRequestParams(message=e.message)
            raise ServerError(e.error, e.message)
        self.session.commit()
        return result.to_json()
    
    def _update_role(self,
               rol: Role,
               name: str,
               description: str,
               enabled: bool
               ) -> Role:

        rol.name = name
        rol.description = description
        rol.enabled = enabled
        
        self.session.add(rol)
        return rol
        