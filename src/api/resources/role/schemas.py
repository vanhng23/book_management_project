from src.bases.schema import (BaseListingSchema, BaseCreatingSchema, BaseUpdatingSchema,
                              fields, StringField, STRING_LENGTH_VALIDATORS)

class GetSchema(BaseListingSchema):
    pass

class PostSchema(BaseCreatingSchema):
    name = StringField(required=True, validate=STRING_LENGTH_VALIDATORS['LONG'])
    
    description = StringField(required=True, validate=STRING_LENGTH_VALIDATORS['LONG'])
    
    enabled = fields.Boolean(allow_none=True)

class PatchSchema(BaseUpdatingSchema):
    name = StringField(required=True, validate=STRING_LENGTH_VALIDATORS['LONG'])
    
    description = StringField(required=True, validate=STRING_LENGTH_VALIDATORS['LONG'])
    
    enabled = fields.Boolean(allow_none=True)