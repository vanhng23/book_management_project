from . import methods

from src.api.base_resource import BaseResource


class PlacesResource(BaseResource):
    endpoint = '/places'
