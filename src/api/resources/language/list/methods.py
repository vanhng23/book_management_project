from src.bases.api.method_handler import MethodHandler
from .schemas import GetSchema

class Get(MethodHandler):
    input_schema_class = GetSchema

    def _handle_api_logic(self):
        print(self.payload['language_code'])
        return list(self.mongo_db.language.find())
