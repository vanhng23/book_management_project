from src.bases.api.resource import Resource


class BookResource(Resource):
    endpoint = '/book'
