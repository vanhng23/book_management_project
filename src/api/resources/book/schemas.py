from src.bases.schema import (BaseListingSchema, BaseCreatingSchema, BaseUpdatingSchema,
                              fields, StringField, STRING_LENGTH_VALIDATORS)

class GetSchema(BaseListingSchema):
    pass

class PostSchema(BaseCreatingSchema):
    code = StringField(required=True, validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])
    
    title = StringField(required=True, validate=STRING_LENGTH_VALIDATORS['LONG'])
    
    description = StringField(required=True, validate=STRING_LENGTH_VALIDATORS['LONG'])
    
    image = StringField(required=True, validate=STRING_LENGTH_VALIDATORS['LONG'])

    enabled = fields.Boolean(allow_none=True)

class PatchSchema(BaseUpdatingSchema):
    code = StringField(required=True, validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])
    
    title = StringField(required=True, validate=STRING_LENGTH_VALIDATORS['LONG'])
    
    description = StringField(required=True, validate=STRING_LENGTH_VALIDATORS['LONG'])
    
    image = StringField(required=True, validate=STRING_LENGTH_VALIDATORS['LONG'])

    enabled = fields.Boolean(allow_none=True)