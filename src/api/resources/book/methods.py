from src.models import Books
from src.bases.api.method_handler import MethodHandler
from src.bases.error.api import BadRequestParams, ServerError
from src.bases.error.core import CoreError
from .schemas import GetSchema, PostSchema, PatchSchema

class Get(MethodHandler):
    input_schema_class = GetSchema
    def _handle_api_logic(self):
        booking = self.session.query(Books).all()
        return [bk.to_json() for bk in booking]
    
class Post(MethodHandler):
    input_schema_class = PostSchema
    def _handle_api_logic(self):
        code = self.payload.get('code')
        title = self.payload.get('title')
        description = self.payload.get('description')
        image = self.payload.get('image')
        enabled = self.payload.get('enabled')
        
        result = self._create(code, title, description, image, enabled)
        self.session.commit()
        return result
        
        
    def _create(self,
               code: str,
               title: str,
               description: str,
               image: str,
               enabled: bool
               ) -> Books:

        book = Books()
        book.code = code
        book.title = title
        book.description = description
        book.image = image
        book.enabled = enabled
        
        self.session.add(book)
        return book.to_json()

class Patch(MethodHandler):
    input_schema_class = PatchSchema
    def _handle_api_logic(self):
        book_id = self.payload.get('id')

        books = self.session.query(Books).filter(
            Books.id == book_id
        ).first()

        if not books:
            raise BadRequestParams(message='Books not found')

        try:
            result = self._update_book(
                books = books,
                code = self.payload.get('code'),
                title = self.payload.get('title'),
                description = self.payload.get('description'),
                image = self.payload.get('image'),
                enabled = self.payload.get('enabled')
            )
        except CoreError as e:
            if e.error == 'InvalidParams':
                raise BadRequestParams(message=e.message)
            raise ServerError(e.error, e.message)
        self.session.commit()
        return result.to_json()
    
    def _update_book(self,
               books: Books,
               code: str,
               title: str,
               description: str,
               image: str,
               enabled: bool
               ) -> Books:

        books.code = code
        books.title = title
        books.description = description
        books.image = image
        books.enabled = enabled
        
        self.session.add(books)
        return books
        