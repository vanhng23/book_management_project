from sqlalchemy import or_

from src.bases.api.method_handler import MethodHandler
from src.models import Currency

from .schemas import GetSchema


class Get(MethodHandler):
    input_schema_class = GetSchema

    def _handle_api_logic(self):
        print('111111')
        scope_type = self.payload.get('scope_type')
        scope_id = self.payload.get('scope_id')
        search_text = self.payload.get('search_text')
        created_before = self.payload.get('created_before')
        created_after = self.payload.get('created_after')

        query = self.session.query(Currency)

        if scope_type:
            query = query.filter(Currency.scope_type.in_(scope_type))

        if scope_id:
            query = query.filter(Currency.scope_id.in_(scope_id))

        if search_text:
            st_params = []
            for st in search_text:
                st_params.append(Currency.code.ilike(f'%{st}%'))
                st_params.append(Currency.name.ilike(f'%{st}%'))
                st_params.append(Currency.country_name.ilike(f'%{st}%'))
            query = query.filter(or_(*st_params))

        if created_before:
            cb_params = []
            for cb in created_before:
                cb_params.append(Currency.created_at <= cb)
            query = query.filter(or_(*cb_params))

        if created_after:
            ca_params = []
            for ca in created_after:
                ca_params.append(Currency.created_at >= ca)
            query = query.filter(or_(*ca_params))

        total = query.count()

        result = []
        for i in query.sort(
            Currency,
            *(self.payload.get('sorts') or [])
        ).paginate(
            page=self.payload.get('page'),
            per_page=self.payload.get('per_page')
        ):
            record = i.to_json(exclude_fields=['description'])
            result.append(record)

        return dict(
            total=total,
            result=result
        )
