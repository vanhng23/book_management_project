from . import methods

from ...base_resource import BaseResource


class HeathCheckResource(BaseResource):

    endpoint = '/health-check'
