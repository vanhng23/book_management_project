from operator import index
from sqlalchemy import (Boolean, Integer, Column, String)
from sqlalchemy.orm import relationship, backref
from src.bases.model import BaseModel
from src.common.constants import STRING_LENGTH


class Books(BaseModel):
    code = Column(String(STRING_LENGTH['UUID4']), nullable = False, index = True)

    title = Column(String(STRING_LENGTH['LONG']), nullable = False, index = True)
    
    description = Column(String(STRING_LENGTH['LONG']), nullable = False, index = True)
    
    image = Column(String(STRING_LENGTH['LONG']))
    
    count_rounding = Column(Integer)
    
    enabled = Column(Boolean, default = True, index = True)

    passenger_books = relationship('PassengerBooks',
                                    backref=backref('books', cascade='all,delete'))