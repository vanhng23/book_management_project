from operator import index
from sqlalchemy import (Column, String, ForeignKey)
from src.bases.model import BaseModel
from src.common.constants import STRING_LENGTH


class PassengerBooks(BaseModel):

    book_id = Column(String(STRING_LENGTH['UUID4']),
                    ForeignKey('books.id'),
                    index=True, nullable=False)
    
    passenger_id = Column(String(STRING_LENGTH['UUID4']),
                    ForeignKey('passenger.id'),
                    index=True, nullable=False)

    status = Column(String(STRING_LENGTH['SHORT']), index = True)