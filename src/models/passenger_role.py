from operator import index
from sqlalchemy import (Column, String, ForeignKey)
from src.bases.model import BaseModel
from src.common.constants import STRING_LENGTH


class PassengerRole(BaseModel):
    role_id = Column(String(STRING_LENGTH['UUID4']),
                    ForeignKey('role.id'),
                    index=True, nullable=False)
    
    passenger_id = Column(String(STRING_LENGTH['UUID4']),
                    ForeignKey('passenger.id'),
                    index=True, nullable=False)