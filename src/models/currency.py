from sqlalchemy import (ForeignKey, Boolean, Integer, Float, Column, String)
from sqlalchemy.orm import backref, relationship

from src.common.constants import STRING_LENGTH
from src.bases.model import BaseModel


class Currency(BaseModel):
    code = Column(String(STRING_LENGTH['UUID4']),
                  nullable=False, index=True)
    name = Column(String(STRING_LENGTH['LONG']),
                  nullable=False, index=True)
    country_name = Column(String(STRING_LENGTH['LONG']))
    symbol = Column(String(STRING_LENGTH['LONG']))
    scope_id = Column(String(STRING_LENGTH['UUID4']),
                      index=True)
    scope_type = Column(String(STRING_LENGTH['EX_SHORT']),
                        index=True, nullable=False)

    rounding = Column(Integer, default=2)

    is_default = Column(Boolean, default=False, index=True)
    enabled = Column(Boolean, default=True, index=True)


class CurrencyConversion(BaseModel):
    scope_id = Column(String(STRING_LENGTH['UUID4']),
                      index=True)
    scope_type = Column(String(STRING_LENGTH['EX_SHORT']),
                        index=True, nullable=False)
    from_code = Column(String(STRING_LENGTH['EX_SHORT']),
                       nullable=False, index=True)
    to_code = Column(String(STRING_LENGTH['EX_SHORT']),
                     nullable=False, index=True)

    rate = Column(Float, nullable=False)
