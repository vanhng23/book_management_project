from operator import index
from sqlalchemy import (Column, String, ForeignKey)
from sqlalchemy.orm import relationship, backref
from src.bases.model import BaseModel
from src.common.constants import STRING_LENGTH


class DeliveryAddress(BaseModel):
    address = Column(String(STRING_LENGTH['LONG']), nullable = False, index = True)

    passenger_id = Column(String(STRING_LENGTH['UUID4']),
                    ForeignKey('passenger.id'),
                    index=True, nullable=False)