from operator import index
from sqlalchemy import (Boolean, Column, String)
from sqlalchemy.orm import relationship, backref
from src.bases.model import BaseModel
from src.common.constants import STRING_LENGTH


class Role(BaseModel):
    name = Column(String(STRING_LENGTH['LONG']), nullable = False, index = True)
    
    description = Column(String(STRING_LENGTH['LONG']), nullable = False, index = True)
    
    enabled = Column(Boolean, default = True, index = True)

    passenger_role = relationship('PassengerRole',
                                   backref=backref('role', cascade='all,delete'))