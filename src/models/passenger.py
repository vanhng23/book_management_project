from operator import index
from sqlalchemy import (Column, String)
from sqlalchemy.orm import relationship, backref
from src.bases.model import BaseModel
from src.common.constants import STRING_LENGTH


class Passenger(BaseModel):
    user = Column(String(STRING_LENGTH['UUID4']), nullable = False, index = True)

    password = Column(String(STRING_LENGTH['SHORT']), nullable = False, index = True)

    first_name = Column(String(STRING_LENGTH['LONG']), nullable = False, index = True)

    last_name = Column(String(STRING_LENGTH['LONG']), nullable = False, index = True)

    email = Column(String(STRING_LENGTH['SHORT']), index = True)

    gender = Column(String(STRING_LENGTH['SHORT']), index = True)

    status = Column(String(STRING_LENGTH['SHORT']), index = True)

    passenger_books = relationship('PassengerBooks',
                                    backref=backref('passenger', cascade='all,delete'))

    passenger_role = relationship('PassengerRole',
                                   backref=backref('passenger', cascade='all,delete'))